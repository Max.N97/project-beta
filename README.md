# CarCar

<!-- <details open>
<summary>Want to ruin the surprise?</summary>
<br>Well, you asked for it!</details> -->

Team:

Max Nguyen: Sales

Brandon Diono: Services


## Instructions to run the application
1. Clone the repository project at: https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta

2. Create the volume, build and run the containers

```
    docker volume create beta-data
    docker-compose build
    docker-compose up
```
3. Confirm that all 7 containers are running

4. Run the application at: http://localhost:3000/


## Design
![excalidraw](diagram.png)


## Overview of all URL endpoints and ports for each service
The service microservice is running on port 8080, sales microservice on 8090, inventory on 8100, and React is on port 3000. Each microservice is connected with the inventory API and integrates the polling mechanism to retrieve/copy certain data over, and React ties everything together in the front-end. To successfully run the application with all working features, make sure all the docker containers are running.


This application utilizes the three APIs which allow users to manage data. **Inventory**, **Service**, and **Sales**. Each API provides its own domain revolving around a car dealership management system by providing different endpoints.

### Inventory API

This API allows users to list all the manufacturers, vehicle models, and registered automobiles that are in inventory. Users are also allowed to create new car manufacturers, vehicle models, and automobiles. This is done through integrating restful methods.

- List manufactureres
    - List all the manufacturers that are created in the database
    - URL: http://localhost:3000/manufacturers
- List vehicle models
    - List all the vehicle models that are created in the database
    - URL: http://localhost:3000/vehicles
- List automobiles
    - List all the automobiles that are created in the database
    - URL: http://localhost:3000/automobiles
- Create a manufacturer
    - Users are able to create a new manufacturer
    - URL: http://localhost:3000/manufacturers/new
- Create a vehicle model
    - Users are able to create a new vehicle model
    - URL: http://localhost:3000/vehicles/new
- Create an automobile
    - Users are able to create/add a new automobile in inventory
    - URL: http://localhost:3000/automobiles/new

### Service API

- List technician
    - This list all the technicians that were created in the database
    - Each technician shown has their employee id, first and last name.
    - URL: http://localhost:3000/technicians
- List service appointments
    - This lists all of the appointments that currently contain a status of "submitted"
    - Each appointment contains the vin number, vip status, customer name, date, time,
      technician employee id and the reasor appointment.
    - URL: http://localhost:3000/appointments
- Create technician
    - This is a form that allows the user to create a technician with an employee id, first
    and last name.
    - URL: http://localhost:3000/technicians/new
- Create appointment
    - This is a form that allows the user to create an appointment with a vin, customer,
    date, time, technician and reason.
    -URL: http://localhost:3000/appointments/new
- Service appointment history
    - This lists all the appointments that contains vin, vip, customer, date, time, technician reason and status.
    - This shows all appointments regardless of their status.
    - URL: http://localhost:3000/appointments/history

### Sales API

This API allows users to list all salespeople, customers, sales, and the sales history of an individual salesperson. Users are also allowed to add a new salesperson, add a new customer, and record a sale. This is done through integrating RESTful methods.

- List salespeople
    - Lists all the salespeople that are created in the database
    - Each salesperson listed shows their employee ID, first and last name.
    - URL: http://localhost:3000/salespeople
- List custommers
    - Lists all the customers that are created in the database
    - Each customer listed shows their full name, phone number, and address.
    - URL: http://localhost:3000/customers
- List sale records
    - Lists all the sales records that are created in the database
    - Each sale listed provides the salesperson's name & employee ID, the customer that purchased, the VIN number of the automobile that was sold, and the price of the automobile
    - URL: http://localhost:3000/sales
- List a salesperson's history
    - Users can choose which salesperson to view their history of sales
    - The history provides the salesperson's name, customer that purchased, the VIN number of the sold automobile, and price of the automobile
    - URL: http://localhost:3000/salespeople/history
- Add a salesperson
    - Users can add a new salesperson
    - URL: http://localhost:3000/salespeople/new
- Add a customer
    - Users can add a new customer
    - URL: http://localhost:3000/customers/new
- Add a sale
    - Users can record a sale made
    - Once recorded, the automobile is marked as sold in inventory
    - URL: http://localhost:3000/sales/new


## Inventory Restful Routes:

### Manufacturers


| Action      | Method      | URL     |
| ----------- | ----------- |---------|
| List manufacturer| GET |http://localhost:8100/api/manufacturers/|
|Create a manufacturer   | POST |	http://localhost:8100/api/manufacturers/|
|Get a specific manufacturer   | GET |	http://localhost:8100/api/manufacturers/:id|
|Update a specific manufacturer| PUT | 	http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer| DELETE | http://localhost:8100/api/manufacturers/:id/ |




To create or update a manufacturer, JSON body only requires the name
```
{
    "name": "BMW"
}
```
When creating, getting, or updating a single manufacturer, it returns the details of that specific manufacturer

returns:
```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "BMW"
}
```
To list all the manufacturers, a dictionary is returned with "manufacturers" key set to a list of all the manufacturers

returns:

```

{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "BMW"
    },

    ...
  ]
}

```

### Vehicle models


| Action      | Method      | URL     |
| ----------- | ----------- |---------|
|List vehicle models|GET|http://localhost:8100/api/models/|
|Create a vehicle model|POST|http://localhost:8100/api/models/|
|Get a specific vehicle model|GET|http://localhost:8100/api/models/:id/|
|Update a specific vehicle model|PUT|http://localhost:8100/api/models/:id/|
|Delete a specific vehicle model|DELETE|http://localhost:8100/api/models/:id/|



When creating a vehicle model, confirm that the value for manufacture_id correlates with a valid/existing manufacturer.

Input:
```
{
  "name": "X4",
  "picture_url": ""https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL. Cannot update the manufacturer. (BMW X4 --> M4)

Input:
```
{
  "name": "M4",
  "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
}
```
From creating, updating, or just getting the details of a specific vehicle model, the details of that specific model is returned

returns:
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "M4",
  "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "BMW"
  }
}
```
For listing all vehicle models, a dictionary is returned with key "models" set to a list of all vehicle models

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "M4",
      "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "BMW"
      }
    }
  ]
}
```

### Automobiles

| Action      | Method      | URL     |
| ----------- | ----------- |---------|
|List automobiles| GET | http://localhost:8100/api/automobiles/|
|Create an automobile| POST | http://localhost:8100/api/automobiles/|
|Get a specific automobile| GET|	http://localhost:8100/api/automobiles/:vin/|
|Update a specific automobile|PUT|	http://localhost:8100/api/automobiles/:vin/|
|Delete a specific automobile|DELETE|http://localhost:8100/api/automobiles/:vin/


Note: Instead of accessing specific automobiles via id, we can access using VIN numbers.


Creating an automobile requires color, year, VIN (unique), and model_id. Confirm that the model_id correlates with a valid/existing vehicle model.

Input:
```
{
  "color": "blue",
  "year": 2020,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

```
Updating the automobile can take color, year, and sold keys. Cannot update model_id.

Input:
```
{
  "color": "black",
  "year": 2020,
  "sold": false
}

```

When creating, updating, or getting the specific automobile, the details of that specific automobile is returned.

Returns:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "black",
  "year": 2020,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "M4",
    "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "BMW"
    }
  },
  "sold": false
}
```
Getting a list of automobiles returns a dictionary with key "autos" set to a list of all the automobiles' information

returns:

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "black",
      "year": 2020,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "M4",
        "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/07/2023-bmw-x4.jpg?w=1024",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "BMW"
        }
      },
      "sold": false
    }
  ]

  ...
}
```


## Service microservice

The service microservice has 3 models, Technician, AuotomobileVO, and Appointment. The AutomobileVO model is responsible for displaying the content of the model Automobile inside of the inventory microservice. The reason for this is so that we are able to poll data from the inventory microservice Automobile model to use in our services AutomobileVO model.

The Technician model ontains `first_name` `last_name` and `employee_id`. This model has all the information we would need for a technician.

The AutomobileVO model contains the `vin` and the `import_href`. These are value objects that we poll from the inventory microservice into our service microservice.

The Appointment model contains `vip` `date_time` `time` `reason` `status` `vin` `customer` and `technician`. These are all fields associated with an appointment being made. The `vip` boolean field refers to if the vehicles `vin` number was at one point within the inventory of vehicles. The `vin` number refers to the vehicle specific number assinged to a car. The `technician` is a foreign key due to the fact that a single `technician` can have multiple appointments assigned to them. The `status` field is responsible for the state of the appointment (submitted, cancelled, or finished).


### Service Microservice API Calls


| Action      | Method | URL |
|-------------|--------|-----|
|List technicians | GET |http://localhost:8080/api/technicians/|
|Create technician| POST | http://localhost:8080/api/technicians/new|

The output for a GET for list of technicians:
```

{
	"technicians": [
		{
			"first_name": "Juice",
			"last_name": "Wrld",
			"employee_id": "JWrld",
			"id": 1
		},
  ]
}
```


Creating a technician requires the fields of first_name, last_name and employee ID.

The input for a POST for creation of technician:
```
{
	"technician": {
		"first_name": "Post",
		"last_name": "Malone",
		"employee_id": "PMalone",
		"id": 2
	}
}
```
The output for the POST for creation of technician:
```
{
	"technician": {
		"first_name": "Post",
		"last_name": "Malone",
		"employee_id": "PMAlone",
		"id": 2
	}
}
```


| Action      | Method | URL |
|-------------|--------|-----|
|List appointments | GET |http://localhost:8080/api/appointments/|
|Create appointment| POST | http://localhost:8080/api/appointments/new|
|Finish appointment | PUT |http://localhost:8080/api/appointments/:id|
|Cancel appointment| PUT | http://localhost:8080/api/appointments/:id|
|Delete appointment | DELETE |http://localhost:8080/api/appointments/:id|


The output for GET request of list of appointments:
```
{
	"appointments": [
		{
			"date_time": "2023-04-28",
			"reason": "flat tire",
			"status": "cancelled",
			"time": "00:10:00",
			"customer": "Brando bando",
			"vin": "0239487290348",
			"vip": true,
			"id": 1,
			"technician": {
				"first_name": "Brandon",
				"last_name": "Dionio",
				"employee_id": "BDionio",
				"id": 2
			},
			"technician_id": 2
		},
  ]
}
```

The input for POST request to create an appointment:
```
{
    "vin": "12345467456",
    "customer": "BD",
		"date_time": "2010-02-01",
		"time": "7:00:00",
		"technician": 1,
		"reason": "Leaking coolant",
		"vip": "False",
		"status":"submitted"
}
```
The output for POST request to create an appointment:

```
{
	"appointment": {
		"date_time": "2010-02-01",
		"reason": "Leaking coolant",
		"status": "submitted",
		"time": "7:00:00",
		"customer": "BD",
		"vin": "12345467456",
		"vip": "False",
		"id": 5,
		"technician": {
			"first_name": "Juice",
			"last_name": "Wrld",
			"employee_id": "JWrld",
			"id": 1
		},
		"technician_id": 1
	}
}
```

The input PUT request for setting status of appointment to finished:

```
{
	"status": "finished"
}
```
The output PUT request for setting status of appointment to finished:
```
{
	"date_time": "2023-04-12",
	"reason": "help my car doesn't work ",
	"status": "submitted",
	"time": "22:34:00",
	"customer": "Shaney Waney",
	"vin": "0239487290348",
	"vip": false,
	"id": 2,
	"technician": {
		"first_name": "Juice",
		"last_name": "Wrld",
		"employee_id": "JWrld",
		"id": 1
	},
	"technician_id": 1
}
```
The input PUT request for setting status of appointment to cancelled:
```
{
	"status": "cancelled"
}
```
The output PUT request for setting status of appointment to cancelled:
```
{
	"date_time": "2010-02-01",
	"reason": "Leaking coolant",
	"status": "created",
	"time": "07:00:00",
	"customer": "BD",
	"vin": "12345467456",
	"vip": false,
	"id": 5,
	"technician": {
		"first_name": "Juice",
		"last_name": "Wrld",
		"employee_id": "JWrld",
		"id": 1
	},
	"technician_id": 1
}
```
The output DELETE request for deletion of appointment:
```
{
	"Message": "Appointment deleted"
}
```


## Sales microservice

For the Sales microservice, there are four models. `Sale`, `Salesperson`, `Customer`, and `AutomobileVO`, where `AutomobileVO` is a value object model that represents the `Automobile` model in the **Inventory Microservice**. This is done through a polling mechanism, where after every minute, `AutomobileVO` creates and/or updates information from the `Automobile` model in the Inventory Microservice. This ensures that the Sales Microservice is always up-to-date with automobile information.

The `Salesperson` model contains `first_name`, `last_name`, and `employee_id` fields.

The `Customer` model contains `first_name`, `last_name`, `address`, and `phone_number` fields.

The `Sales` model contains `price`, `automobile`, `salesperson`, and `customer` fields.

 - The `automobile` field is a foreign key to the `AutomobileVO` model. This relationship can be defined as one-to-one as there can only be one sale for each registered automobile in inventory. If a customer wants to buy more than one automobile, it would be recorded as a seperate sale with a different automobile instance.

  - The `salesperson` field is another foreign key to the `salesperson` model. This is a one-to-many relationship where a single salesperson can make many sales (if they're good 👀).

  - The `customer` field is another foreign key to the `customer` model. This is a one-to-many relationship where a customer can purchase many times, making many sale instances.

The `AutomobileVO` model contains only the `import_vin` field.
  - `import_vin` objects are essentially a copy of the `vin` (Inventory Microservice) objects

### Sales microservice API calls

| Action      | Method      | URL     |
| ----------- | ----------- |---------|
|List all salespeople| GET | http://localhost:8090/api/salespeople/ |
| Create a salesperson| POST | http://localhost:8090/api/salespeople/ |
| Delete a salesperson | DELETE | http://localhost:8090/api/salespeople/:id|

Getting the list of all salespeople returns a dictionary with a "salespeople" key set to a list of salespeople created in the database.

Returns:
```
{
	"salespeople": [
		{
			"first_name": "Callista",
			"last_name": "Nackviseth",
			"id": 1,
			"employee_id": "IDQ23G"
		},

      ...
	]
}
```

When creating a salesperson, first_name, last_name, and employee_id is required. Employee ID must not exceed 6 characters.

Input:
```
{
	"first_name": "John",
	"last_name": "Cena",
	"employee_id": "SSFEF2"
}
```
Once created, the details of that salesperson is returned

Returns:
```
{
	"first_name": "John",
	"last_name": "Cena",
	"id": 2,
	"employee_id": "SSFEF2"
}
```
Deleting a salesperson returns message displaying if the deletetion was a success

Returns:
```
{
	"deleted": true
}
```
If the id provided in the URL was invalid, it returns a false value with a 400 bad reqeust status

Returns
```
{
	"deleted": false
}
```

| Action      | Method      | URL     |
| ----------- | ----------- |---------|
|List all customers| GET | http://localhost:8090/api/customers/ |
| Create a customers| POST | http://localhost:8090/api/customers/ |
| Delete a customers | DELETE | http://localhost:8090/api/customers/:id|

Getting the list of all customers returns a dictionary with a "customers" key set to a list of customers created in the database.

Returns:
```
{
	"customers": [
		{
			"first_name": "Willem",
			"last_name": "Dafoe",
			"address": 1,
			"phone_number": "55555555555"
      "id": 1
		},

      ...
	]
}
```

Creating a customer requires first_name, last_name, address, phone_number (as a string)

Input:

```
{
	"first_name": "Mike",
	"last_name": "Tyson",
	"address": "1190 Pike St. Seattle WA",
	"phone_number": "5555555555"
}
```
Returns the specific customer created with id
```
{
	"first_name": "Mike",
	"last_name": "Tyson",
	"address": "1190 Pike St. Seattle WA",
	"phone_number": "5555555555"
  "id": 2
}
```
Deleting a customer returns message displaying if the deletetion was a success

Returns:
```
{
	"deleted": true
}
```
If the id provided in the URL was invalid, it returns a false value with a 400 bad request status

Returns
```
{
	"deleted": false
}
```

| Action      | Method      | URL     |
| ----------- | ----------- |---------|
|List all sales| GET | http://localhost:8090/api/sales/ |
| Create a sale| POST | http://localhost:8090/api/sales/ |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id|

Getting the list of all sales returns a dictionary with a "sales" key set to a list of sales created in the database.

Returns:
```
{
	"sales": [
		{
			"first_name": "Willem",
			"last_name": "Dafoe",
			"address": 1,
			"phone_number": "55555555555"
      "id": 1
		},

      ...
	]
}
```
Creating a sales requires price (string), automobile (automobile VIN), salesperson (id), and customer (id).

NOTE: Creating through insomnia, the automobile VIN is case sensitive in the JSON body.

Input:

```
{
	"price": "600000",
	"automobile": "1GNDX03E03D188446",
	"salespersion": 1,
	"customer": 1
}
```
Returns the specific sales created with the following deatails

Returns:
```
{
	"id": 1,
	"price": "600000",
	"salesperson": {
		"first_name": "John",
		"last_name": "Cena",
		"id": 1,
		"employee_id": "SSFEF2"
	},
	"customer": {
		"first_name": "Willem",
		"last_name": "Dafoe",
		"address": "random address",
		"phone_number": "333333333333",
		"id": 1
	},
	"automobile": {
		"import_vin": "1GNDX03E03D188446",
		"id": 1
	}
}
```

Deleting a sale returns message displaying if the deletetion was a success

Returns:
```
{
	"deleted": true
}
```
If the id provided in the URL was invalid, it returns a false value with a 400 bad request status

Returns
```
{
	"deleted": false
}
