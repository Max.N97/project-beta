from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    vip = models.BooleanField(default=False)
    date_time = models.DateField()
    time = models.TimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100, default="submitted")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f"{self.customer} - {self.vin}"
