from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
from django.core.serializers.json import DjangoJSONEncoder
import json


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin"
    ]


class TechnicianEncoder(DjangoJSONEncoder,  ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentEncoder(DjangoJSONEncoder, ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "time",
        "customer",
        "vin",
        "vip",
        "id",
        "technician"
    ]

    def get_extra_data(self, o):
        return {"technician_id": o.technician.id}

    encoders = {
        "technician": TechnicianEncoder(),
    }

    #
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["time"] = instance.time.strftime("%H:%M:%S")
        data["date"] = instance.date_time.strftime("%Y-%m-%d")
        return data


@require_http_methods(["GET", "PUT", "DELETE"])
def show_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
        )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({
                "message": "Technician is not in the database"
            })
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": list(technicians)},
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technicians},
            encoder=TechnicianEncoder,
            safe=True,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        print(appointments)
        return JsonResponse(
            {"appointments": list(appointments)},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            techs = Technician.objects.get(id=content["technician"])
            content["technician"] = techs

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "That technician is not in the data base"},
                status=400
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False
        )


@require_http_methods(["DELETE"])
def delete_appointments(request, pk):
    appointments = Appointment.objects.get(id=pk)
    appointments.delete()
    return JsonResponse(
        {"Message": "Appointment deleted"}
    )


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    content = json.loads(request.body)
    appointment = Appointment.objects.get(id=id)
    Appointment.objects.filter(id=id).update(**content)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    content = json.loads(request.body)
    appointment = Appointment.objects.get(id=id)
    Appointment.objects.filter(id=id).update(**content)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )
