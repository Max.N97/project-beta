from django.urls import path
from .views import list_technicians
from .views import show_technicians
from .views import list_appointments
from .views import delete_appointments
from .views import cancel_appointment
from .views import finish_appointment

urlpatterns = [
    path(
        "technicians/",
        list_technicians,
        name="list_technicians"
    ),
    path(
        "technicians/<int:id>",
        show_technicians,
        name="show_technicians"
    ),
    path(
        "appointments/",
        list_appointments,
        name="list_appointments"
    ),
    path(
        "appointments/<int:pk>",
        delete_appointments,
        name="delete_appointments"
    ),
    path(
        "appointments/<int:id>/cancel",
        cancel_appointment,
        name="cancel_appointment"
    ),

    path(
        "appointments/<int:id>/finish",
        finish_appointment,
        name="finish_appointment"
    )
]
