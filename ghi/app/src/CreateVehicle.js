import React, { useEffect, useState } from 'react';

function CreateVehicle() {
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [modelname, setModelName] = useState('');
    const [picture_url, setPictureUrl] = useState('');

    const handleModelName = (event) => {
        const value = event.target.value;
        setModelName(value)
    }
    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = modelname;
        data.picture_url = picture_url;
        data.manufacturer_id = manufacturer;

        const newVehicleUrl = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(newVehicleUrl, fetchConfig);
        if (response.ok) {

            setModelName('');
            setPictureUrl('');
            setManufacturer('');
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/"

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle">
                        <div>
                            <label htmlFor="model_name">Model Name</label>
                            <input
                                value={modelname}
                                onChange={handleModelName}
                                required
                                type="text"
                                placeholder="Model name"
                                id="model_name"
                            />
                        </div>
                        <div>
                            <label htmlFor="picture_url">Picture Url</label>
                            <input
                                value={picture_url}
                                onChange={handlePictureUrl}
                                required
                                type="text"
                                placeholder="Picture Url"
                                id="picture_url"
                            />
                        </div>
                        <div>
                            <select onChange={handleManufacturer}
                            value={manufacturer}
                            name="manufacturer"
                            required
                            className="form-select"
                            >
                                <option value="">Choose a manufacturer...</option>
                                {manufacturers?.map((manufacturer) => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateVehicle
