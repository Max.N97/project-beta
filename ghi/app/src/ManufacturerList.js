import { Link } from "react-router-dom";
import React, { useEffect, useState } from 'react'

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map((manufacturer, index) => {
                        return (
                            <tr key={manufacturer.name + index}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/manufacturers/new" className="btn btn-primary btn-lg px-4 gap-3">Add a manufacturer</Link>
            </div>
        </>
    )
}

export default ManufacturerList;
