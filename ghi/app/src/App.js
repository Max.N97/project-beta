import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleList from './VehicleList'
import CreateVehicle from './CreateVehicle'
import ListAutomobile from './ListAutomobile'
import ManufacturerList from './ManufacturerList';
import CreateManufacturer from './CreateManufacturer';
import CreateAutomobile from './CreateAutomobile';
import SalespeopleList from './sales/SalespeopleList';
import CreateSalesperson from './sales/CreateSalesperson';
import ListCustomers from './sales/ListCustomers';
import CreateCustomer from './sales/CreateCustomer';
import ListSales from './sales/ListSales';
import CreateSale from './sales/CreateSales';
import SalespersonHistory from './sales/SalespersonHistory';
import ListTechnician from './services/ListTechnician';
import CreateTechnician from './services/CreateTechnician';
import ListAppointment from './services/ListServiceAppointment';
import CreateAppointment from './services/CreateServiceAppointment';
import ServiceHistory from './services/ServiceHistory';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route path="" element={<MainPage />} />

            <Route path="vehicles">
              <Route path="" index element={<VehicleList />}/>
              <Route path="new" element={<CreateVehicle />} />
            </Route>

            <Route path="automobiles">
              <Route path="" index element={<ListAutomobile />} />
              <Route path="new" element={<CreateAutomobile />} />
            </Route>

            <Route path="manufacturers">
              <Route path="" element={<ManufacturerList />} />
              <Route path="new" element={<CreateManufacturer />} />
            </Route>

            <Route path="salespeople">
              <Route path="" index element={<SalespeopleList />} />
              <Route path="new" element={<CreateSalesperson />} />
              <Route path="history" element={<SalespersonHistory />} />
            </Route>

            <Route path="customers">
              <Route path="" index element={<ListCustomers />} />
              <Route path="new" element={<CreateCustomer />} />
            </Route>

            <Route path="sales">
              <Route path="" index element={<ListSales />} />
              <Route path="new" element={<CreateSale />} />
            </Route>

            <Route path="vehicles">
              <Route path="" index element={<VehicleList />}/>
              <Route path="new" element={<CreateVehicle />} />
            </Route>

            <Route path="automobiles">
              <Route path="" index element={<ListAutomobile />} />
              <Route path="new" element={<CreateAutomobile />} />
            </Route>

            <Route path="manufacturers">
              <Route path="" element={<ManufacturerList />} />
              <Route path="new" element={<CreateManufacturer />} />
            </Route>

            <Route path="technicians">
              <Route path="" index element={<ListTechnician />} />
              <Route path="new" index element={<CreateTechnician />} />
            </Route>

            <Route path="appointments">
              <Route path="" index element={<ListAppointment />} />
              <Route path="new" index element={<CreateAppointment />} />
              <Route path="history" index element={<ServiceHistory />} />
            </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}





export default App;
