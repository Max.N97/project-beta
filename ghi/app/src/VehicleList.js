import { Link } from "react-router-dom"
import React, { useEffect, useState } from 'react';

function VehicleList() {

    const [models, setVehicles] = useState([]);
    const fetchData = async () => {

        const url = "http://localhost:8100/api/models/"

        const response = await fetch (url)
        if (response.ok) {
            const data = await response.json();
            setVehicles(data.models)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models?.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer.name}</td>
                                <td> <img src={ vehicle.picture_url } alt="" width="500"></img></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/vehicles/new" className="btn btn-primary btn-lg px-4 gap-3">Add a vehicle</Link>
            </div>
        </>
    )
}

export default VehicleList
