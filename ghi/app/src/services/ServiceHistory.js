import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

function ServiceHistory() {

    const [appointments, setAppointments] = useState([]);
    const [searchVin, setSearchVin] = useState('');
    const [resetAppointments, setResetAppointments] = useState('');

    const fetchData = async () => {
        const apptUrl = "http://localhost:8080/api/appointments/"

        const response = await fetch(apptUrl)
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
            setResetAppointments(data.appointments)
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setSearchVin(value)
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const filteredVin = appointments.filter(appointment =>
        appointment.vin.includes(searchVin));
        setAppointments(filteredVin);
    }

    const handleReset = (event) => {
        event.preventDefault();
        setSearchVin('');
        setAppointments(resetAppointments)
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Service History</h1>
                        <form onSubmit={handleSubmit} id="filter-vin">
                            <div>
                            <label htmlFor="search-input">Search by VIN</label>
                            <input
                            value={searchVin}
                            onChange={handleVinChange}
                            required
                            type="text"
                            placeholder="Search by vin..."
                            id="search-input"
                            />
                            <button type="submit" className="btn btn-primary">Search</button>
                            <button onClick={handleReset} className="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Vin</th>
                                    <th>Is VIP?</th>
                                    <th>Customer</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Technician</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {appointments?.map(appointment => {
                                    return (
                                        <tr key={appointment.id}>
                                            <td style={{ textTransform: 'uppercase' }}>{appointment.vin}</td>
                                            <td>{appointment.vip ? "Yes":"No"}</td>
                                            <td>{appointment.customer}</td>
                                            <td>{appointment.date_time}</td>
                                            <td>{appointment.time}</td>
                                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.status}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                            <Link to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Create Appointment</Link>
                        </div>
                </div>
            </div>
        </>
    )
}

export default ServiceHistory;
