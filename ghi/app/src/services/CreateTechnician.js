import React, { useState } from 'react';


function CreateTechnician() {


    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value)
    }

    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const handleEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const newTechnicianUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(newTechnicianUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician">
                        <div>
                            <label htmlFor="first_name">First Name</label>
                            <input
                                value={first_name}
                                onChange={handleFirstName}
                                required
                                type="text"
                                placeholder="First Name"
                                id="first_name"
                            />
                        </div>
                        <div>
                            <label htmlFor="last_name">Last Name</label>
                            <input
                            value={last_name}
                            onChange={handleLastName}
                            required
                            type="text"
                            placeholder="Last Name"
                            id="last_name"
                            />
                        </div>
                        <div>
                            <label htmlFor="employee_id">Employee Id</label>
                            <input
                            value={employee_id}
                            onChange={handleEmployeeId}
                            minLength="6" maxLength="6"
                            required
                            type="text"
                            placeholder="Employee Id"
                            id="employee_id"
                            style={{ textTransform: 'uppercase' }}
                            />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateTechnician;
