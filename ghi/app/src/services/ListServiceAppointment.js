import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

function ListAppointment() {

    // Grab the appointment data
    const [appointments, setAppointments] = useState([]);
    const fetchData = async () => {
        const apptUrl = "http://localhost:8080/api/appointments/"

        const response = await fetch(apptUrl)
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    // Handle what happens when cancel is clicked
    const handleCancelAppointment = async (id) => {
        const apptCancelUrl = `http://localhost:8080/api/appointments/${id}/cancel`;
        const response = await fetch(apptCancelUrl, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ status: 'cancelled' }),
        })
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    // what happens when finish is clicked
    const handleFinishAppointment = async (id) => {
        const apptFinishUrl = `http://localhost:8080/api/appointments/${id}/finish`;
        const response = await fetch(apptFinishUrl, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ status: 'finished' }),
        })
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {/* optional chaining issue? */}
                    {appointments?.map(appointment => {
                        if (appointment.status === 'submitted') {
                        return (
                            <tr key={appointment.id}>
                                <td style={{ textTransform: 'uppercase' }}>{appointment.vin}</td>
                                <td>{appointment.vip ? "Yes":"No"}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.employee_id}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    {appointment.status !== "cancelled" && appointment.status !== "finished" ? (
                                        <>
                                            <button onClick={() => {handleCancelAppointment(appointment.id)}}>Cancel</button>
                                            <button onClick={() => {handleFinishAppointment(appointment.id)}}>Finish</button>
                                        </>
                                    ) : null}
                                </td>
                            </tr>
                        );
                        } else {
                            return null;
                        }
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                <Link to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Create Appointment</Link>
            </div>
        </>
    )
}

export default ListAppointment;
