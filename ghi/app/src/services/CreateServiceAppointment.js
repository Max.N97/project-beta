import React, { useEffect, useState } from 'react';

function CreateAppointment() {

    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date_time, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [vip] = useState('');

    const handleTechnician = (event) => {
        const value = event.target.value;
        setTechnician(value)
    }

    const handleVin = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const handleCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }

    const handleDate = (event) => {
        const value = event.target.value;
        setDate(value)
    }

    const handleTime = (event) => {
        const value = event.target.value;
        setTime(value)
    }

    const handleReason = (event) => {
        const value = event.target.value;
        setReason(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = date_time;
        data.time = time;
        data.reason = reason;
        data.technician = technician;
        data.vip = vip;

        const automobileData = "http://localhost:8100/api/automobiles/"
        const pull = await fetch(automobileData)
        const info = await pull.json()
        const autoData = info.autos

        // for loop in order to check if VIN number is has already been in inventory.
        let vinFind = false;
        for (let i=0; i < autoData.length; i++) {
            if (vin===autoData[i].vin) {
                vinFind = true;
                break;
            }
        }
        data.vip = vinFind

        const newAppointmentUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(newAppointmentUrl, fetchConfig);
        if (response.ok) {
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setReason('');
            setTechnicians([]);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment">
                        <div>
                            <label htmlFor="vin">Vin</label>
                            <input
                                style={{ textTransform: 'uppercase' }}
                                value={vin}
                                onChange={handleVin}
                                minLength="17"
                                maxLength="17"
                                required
                                type="text"
                                placeholder="Vin"
                                id="vin"
                                />
                        </div>
                        <div>
                            <label htmlFor="customer">Customer</label>
                            <input
                                value={customer}
                                onChange={handleCustomer}
                                required
                                type="text"
                                placeholder="Customer"
                                id="customer"
                                />
                        </div>
                        <div>
                            <label htmlFor="date">Date</label>
                            <input
                                value={date_time}
                                onChange={handleDate}
                                required
                                type="date"
                                placeholder="Date"
                                id="date"
                                />
                        </div>
                        <div>
                            <label htmlFor="time">Time</label>
                            <input
                                value={time}
                                onChange={handleTime}
                                required
                                type="time"
                                placeholder="Time"
                                id="time"
                                />
                        </div>
                        <div>
                            <select onChange={handleTechnician}
                            value={technician.id}
                            name="technician"
                            id="technician"
                            required
                            className="form-select"
                            >
                                <option value="">Choose a Technician</option>
                                    {technicians?.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.first_name} {technician.last_name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div>
                            <label htmlFor="reason">Reason</label>
                            <input
                                value={reason}
                                onChange={handleReason}
                                required
                                type="reason"
                                placeholder="reason"
                                id="reason"
                                />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateAppointment;
