import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

function ListTechnician() {

    const [technicians, setTechnicians] = useState([]);
    const fetchData = async () => {
        const technicianDataUrl = "http://localhost:8080/api/technicians/"

        const response = await fetch(technicianDataUrl)
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1>Technicians</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians?.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td style={{ textTransform: 'uppercase' }}>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                <Link to="/technicians/new" className="btn btn-primary btn-lg px-4 gap-3">Add a technician</Link>
            </div>
        </>
    )
}

export default ListTechnician;
