import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

function ListAutomobile() {
    const [autos, setAutomobiles] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/"

        const response = await fetch (url)
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div className="row">
                <div className="col">
                    <h1>Automobiles</h1>
                </div>
            </div>
            <table className="table table-striped">
                <thead key="">
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos?.map(automobile => {
                        return (
                        <tr key={automobile.id}>
                            <td style={{ textTransform: 'uppercase' }}>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.sold ? "Yes": "No"}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                <Link to="/automobiles/new" className="btn btn-primary btn-lg px-4 gap-3">Add an automobile</Link>
            </div>
        </>
    )
}

export default ListAutomobile
