import React, { useEffect, useState } from 'react'

function CreateSale() {
    const [autos, setAutos] = useState([]);
    const [vin, setVin] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState([]);
    const [price, setPrice] = useState([]);

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    // Fetching all the automobile objects for the dropdown,
    // If response is good, filter out the unsold autos to populate
    const fetchDataAutos = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const filteredUnsold = data.autos.filter(auto => auto.sold !== true)
            setAutos(filteredUnsold)
        }
    }

    // Fetching all salespeople objects for dropdown
    const fetchDataSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    // Fetching all customer objects for dropdown
    const fetchDataCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchDataAutos();
        fetchDataSalespeople();
        fetchDataCustomers();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.price = price;
        data.automobile = vin;
        data.customer = customer;
        data.salesperson = salesperson;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const automobileResponse = await fetch(saleUrl, fetchConfig);
        if (automobileResponse.ok) {
            setPrice('');
            setAutos([]);
            setCustomers([]);
            setSalespeople([]);
        }

        const autoUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
        const fetchAutoConfig = {
            method: "put",
            body: JSON.stringify({"sold": true }),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const soldResponse = await fetch(autoUrl, fetchAutoConfig);
        if (soldResponse.ok) {
            console.log(data)
        }

    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div>
                        <h1>Add a Sale</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <div className="mb-3">
                                    <select onChange={handleVinChange} name="vin" required id="vin" className="form-select">
                                    <option>Choose an automobile VIN..</option>
                                    {autos?.map(auto => {
                                        return (
                                            <option key={auto.id} value={auto.vin}>
                                                {auto.vin}
                                            </option>
                                        )
                                    })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" className="form-select">
                                    <option>Choose a salesperson..</option>
                                    {salespeople?.map(person => {
                                        return (
                                            <option key={person.id} value={person.id}>
                                                { person.first_name + " " + person.last_name }
                                            </option>
                                        )
                                    })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleCustomerChange} name="customer" required id="customer" className="form-select">
                                    <option>Choose a customer..</option>
                                    {customers?.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                { customer.first_name + " " + customer.last_name }
                                            </option>
                                        )
                                    })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handlePriceChange} value={price} placeholder="price" required
                                    type="number" name="price" id="price"
                                    className="form-control"/>
                                    <label htmlFor="price">Price...</label>
                                </div>
                            </div>
                        <div>
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default CreateSale;
