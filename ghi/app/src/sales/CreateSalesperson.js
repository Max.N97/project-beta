import React, { useState } from 'react'

function CreateSalesperson () {
    const [employeeId, setEmployeeId] = useState([]);
    const [firstName, setFirstName] = useState([]);
    const [lastName, setLastName] = useState([]);

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.employee_id = employeeId;
        data.first_name = firstName;
        data.last_name = lastName;

        const salesPersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            setEmployeeId('');
            setFirstName('');
            setLastName('');
        };
    };

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div>
                        <h1>Add a Salesperson</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} style={{ textTransform: 'uppercase' }} minLength="6" maxLength="6" value={employeeId} placeholder="Employee ID" required
                            type="text" name="employee_id" id="employee_id"
                            className="form-control"/>
                            <label htmlFor="employee_id">Employee ID..</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} placeholder="First name" required
                            type="text" name="first_name" id="first_name"
                            className="form-control"/>
                            <label htmlFor="first_name">First Name..</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={lastName} placeholder="Last name" required
                            type="text" name="last_name" id="last_name"
                            className="form-control"/>
                            <label htmlFor="last_name">Last name..</label>
                        </div>
                        <div>
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default CreateSalesperson;
