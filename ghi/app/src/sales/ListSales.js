import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ListSales() {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale, index) => {
                        return (
                            <tr key={sale.id + index}>
                                <td style={{ textTransform: 'uppercase' }}>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.salesperson.first_name }</td>
                                <td>{ sale.customer.first_name + " " + sale.customer.last_name }</td>
                                <td>{ sale.automobile.import_vin }</td>
                                <td>${ sale.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/sales/new" className="btn btn-primary btn-lg px-4 gap-3">Add a sale</Link>
            </div>
        </>
    )
}

export default ListSales;
