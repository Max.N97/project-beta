import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ListCustomers() {
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((customer, index) => {
                        return (
                            <tr key={customer.id + index}>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.phone_number }</td>
                                <td>{ customer.address }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/customers/new" className="btn btn-primary btn-lg px-4 gap-3">Add a customer</Link>
            </div>
        </>
    )
}

export default ListCustomers;
