import React, { useEffect, useState } from 'react'

function SalespersonHistory () {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const [sales, setSales] = useState([]);

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    // drop down menu of salespeople
    const fetchSalespeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    // now fetch sales data for filtering
    const fetchSalesData = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl)
        if (response.ok) {
    // once response is good, we can access to iterate and filter through each sale within sales
            const data = await response.json();
    // filter out, if that current salesperson id for that specific sale matches id of that current salesperson state
    // update that sale state
            const filteredSales = data.sales.filter(sale => sale.salesperson.id === parseInt(salesperson))
            setSales(filteredSales)
        }
    }


    useEffect(() => {
        fetchSalespeopleData();
    }, []);

    useEffect(() => {
        fetchSalesData();
    }, [salesperson]);

    return (
            <>
                <div>
                    <h1>Salesperson History</h1>
                </div>
                    <div className="mb-3">
                        <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" className="form-select">
                        <option>Choose a salesperson..</option>
                        {salespeople?.map(person => {
                            return (
                                <option key={person.id} value={person.id}>
                                    {person.first_name + " " + person.last_name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <div>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Salesperson</th>
                                    <th>Employee ID</th>
                                    <th>Customer</th>
                                    <th>VIN</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {sales.map((sale, index) => {
                                    return (
                                        <tr key={sale.id + index}>
                                            <td>{ sale.salesperson.first_name }</td>
                                            <td style={{ textTransform: 'uppercase' }}>{ sale.salesperson.employee_id}</td>
                                            <td>{ sale.customer.first_name }</td>
                                            <td style={{ textTransform: 'uppercase' }}>{ sale.automobile.import_vin }</td>
                                            <td>${ sale.price }</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
            </>
    )
}

export default SalespersonHistory;
