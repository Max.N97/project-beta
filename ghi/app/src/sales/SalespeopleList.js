import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';


function SalespeopleList () {
    const [salespeople, setSalespeople] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map((person, index) => {
                    return (
                        <tr key={person.employee_id + index}>
                            <td style={{ textTransform: 'uppercase' }}>{ person.employee_id }</td>
                            <td>{ person.first_name }</td>
                            <td>{ person.last_name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/salespeople/new" className="btn btn-primary btn-lg px-4 gap-3">Add a salesperson</Link>
        </div>
    </>
    )
}

export default SalespeopleList;
