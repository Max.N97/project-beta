import React, { useEffect, useState } from 'react'


function CreateAutomobile() {
    const [models, setModels] = useState([]);
    const [color, setColor] = useState([]);
    const [year, setYear] = useState([]);
    const [vin, setVin] = useState([]);
    const [model, setModel] = useState([]);


    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        console.log(data);

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const automobileResponse = await fetch(automobileUrl, fetchConfig);
        if (automobileResponse.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModels([]);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div>
                        <h1>Add an automobile to inventory</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} value={color} placeholder="Colorsss" required
                                type="text" name="color" id="color"
                                className="form-control"/>
                                <label htmlFor="color">Color...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleYearChange} value={year} placeholder="Year" required
                                type="text" name="year" id="year"
                                className="form-control"/>
                                <label htmlFor="year">Year..</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} style={{ textTransform: 'uppercase' }} maxLength="17" minLength="17" value={vin} placeholder="Vin" required
                                type="text" name="vin" id="vin"
                                className="form-control"/>
                                <label htmlFor="vin">VIN..</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleModelChange} value={model.id} name="model" required id="model" className="form-select">
                                <option>Choose a model..</option>
                                {models?.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                        </div>
                        <div>
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateAutomobile;
