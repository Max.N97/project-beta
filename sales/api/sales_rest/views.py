from django.shortcuts import render
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_vin",
        "id",
    ]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "id",
        "employee_id"
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile"
    ]
    encoders= {
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()

        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )

    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_salesperson (request, pk):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=pk).delete()

    # If unable to delete a salesperson, return with a 400 status
        if count <= 0:
            return JsonResponse(
                {"deleted": count > 0},
                status=400
                )
        else:
            return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()

        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )

    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def delete_customer (request, pk):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        if count <= 0:
            return JsonResponse(
                {"deleted": count > 0},
                status=400
                )
        else:
            return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()

        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
# The content in the JSON body in insomnia:
        # The input VALUE of "automobile" key should be a viable VIN number from created automobiles
            vin = content["automobile"]
        # Getting the instance where the imported VIN matches the input VIN
            automobile = AutomobileVO.objects.get(import_vin=vin)
        # Now assigning the automobile key to that specific AutoVO instance
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid auto VIN"},
                status=400,
            )

        try:
# Input value should be a viable ID integer that matches the ID of a specific salesperson instance
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesListEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_sale (request, pk):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        if count <= 0:
            return JsonResponse(
                {"deleted": count > 0},
                status=400
                )
        else:
            return JsonResponse({"deleted": count > 0})
